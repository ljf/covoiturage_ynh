covoiturage_ynh
===============

Covoiturage-libre package for YunoHost 
https://github.com/SebGit/covoiturage-libre

WARNING: License seams to be Creative commons by-nc-sa http://creativecommons.org/licenses/by-nc-sa/3.0/fr/ so commercial use is forbidden. I wrote to the author to know more.
